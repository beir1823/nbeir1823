package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class TestSuite_1 {

    public static BibliotecaCtrl controller;
    public static Carte carte = new Carte();

    public static void setItems()
    {
        carte.setTitlu("Harry Potter");
        carte.setAnAparitie("2006");
        carte.setEditura("Humanitas");

        List<String> referenti = new ArrayList<String>();
        referenti.add("AutorA");
        referenti.add("AutorB");
        referenti.add("AutorC");
        List<String> cuvCheie = new ArrayList<String>();
        cuvCheie.add("SF");
        cuvCheie.add("Drama");

        carte.setReferenti(referenti);
        carte.setCuvinteCheie(cuvCheie);
    }

    @BeforeClass
    public static void instantiate()
    {
        controller = new BibliotecaCtrl(new CartiRepoMock());
        System.out.println("Once!!");

        setItems();
    }

    @Test
    public void testAddValidBook()
    {
        try
        {
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Book added!", e.getMessage());
        }
    }

    @Test
    public void testValidBookTitle()
    {
        try
        {
            carte.setTitlu("Piratii din caraibe");
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Book added!", e.getMessage());
        }
    }

    @Test
    public void testInvalidBookTitle()
    {
        try
        {
            carte.setTitlu("Harry2 Potter");
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Titlu invalid! \n", e.getMessage());
        }
        setItems();
    }

    @Test
    public void testValidPublishingHouse()
    {
        try
        {
            carte.setEditura("Litera");
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Book added!", e.getMessage());
        }

    }

    @Test
    public void testInvalidPublishingHouse()
    {
        try
        {
            carte.setEditura("Litera10");
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Editura invalida! \n", e.getMessage());
        }
        setItems();
    }

    @Test
    public void testInvalidAnAparitie()
    {
        try
        {
            carte.setAnAparitie("20wwe12t");
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("An aparitie invalid! \n", e.getMessage());
        }
        setItems();
    }

    @Test
    public void testInvalidReferenti()
    {
        try
        {
            carte.setReferenti(new ArrayList());
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Lista autori vida! \n", e.getMessage());
        }
        setItems();
    }

    @Test
    public void testInvalidCuvinteCheie()
    {
        try
        {
            carte.setCuvinteCheie(new ArrayList());
            controller.adaugaCarte(carte);
        }
        catch (Exception e)
        {
            assertEquals("Lista cuvinte cheie vida! \n", e.getMessage());
        }
        setItems();
    }
}
