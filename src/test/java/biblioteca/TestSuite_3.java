package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class TestSuite_3 {

    public static BibliotecaCtrl controller;

    @BeforeClass
    public static void instantiate()
    {
        controller = new BibliotecaCtrl(new CartiRepoMock());
        System.out.println("Once!!");
    }

    @Test
    public void testSortValid()
    {
        try
        {
            assertEquals(3, controller.getCartiOrdonateDinAnul("1948").size());

        }
        catch (Exception e)
        {
        }
    }

    @Test
    public void testSortInvalid()
    {
        try
        {
            assertEquals(0, controller.getCartiOrdonateDinAnul("10").size());
        }
        catch (Exception e)
        {
        }
    }
}
