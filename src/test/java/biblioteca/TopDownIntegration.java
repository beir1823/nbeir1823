package lab04;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/*
* modulul A: adaugarea unei noi carti (titlu, autor/autori, an aparitie, editura, cuvinte cheie);
* modulul B: cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
* modulul C: afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.
* modulul P: aplicatia de tip consola
 */
public class TopDownIntegration {

    private static CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void  initialize(){
        cartiRepoMock = new CartiRepoMock();
    }

    // testare unitara: A
    @Test
    public void unitTestA() throws Exception {
        int count = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Test;Manu;1992;Corint;awesome"));
        Assert.assertEquals(count + 1, cartiRepoMock.getCarti().size());
    }

    // integrare B (se testeaza A si B)
    // scenariu de executie: P -> A -> B
    @Test
    public void integrareB() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Test;bacovia;1992;Corint;awesome"));
        List<Carte> result = cartiRepoMock.cautaCarte("Bac");
        Assert.assertEquals(1, result.size());
    }

    // integrare C (se testeaza A, B si C)
    // scenariu de testare: P -> C -> B -> A
    @Test
    public void integrareC() throws Exception {
        // C
        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul("1948");
        Assert.assertEquals(3, result.size());
        Assert.assertEquals("Enigma Otiliei", result.get(1).getTitlu());
        Assert.assertEquals(2, result.get(1).getCuvinteCheie().size());

        // B
        List<Carte> result2 = cartiRepoMock.cautaCarte("Ion");
        Assert.assertEquals(2, result2.size());

        // A
        int size = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Test;bacovia;1992;Corint;awesome"));
        Assert.assertEquals(size + 1, cartiRepoMock.getCarti().size());
    }
}
