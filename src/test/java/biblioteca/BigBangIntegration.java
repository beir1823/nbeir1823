package lab04;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/*
* modulul A: adaugarea unei noi carti (titlu, autor/autori, an aparitie, editura, cuvinte cheie);
* modulul B: cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
* modulul C: afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.
* modulul P: aplicatia de tip consola
 */
public class BigBangIntegration {

    private static CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void  initialize(){
        cartiRepoMock = new CartiRepoMock();
    }

    // testare unitara: A
    @Test
    public void unitTestA() throws Exception {
        Carte carte = new Carte();
        carte.setTitlu("Harap-Alb");
        List<String> autori = new ArrayList();
        autori.add("Ion Creanga");
        autori.add("Mihai Eminescu");
        carte.setReferenti(autori);
        carte.setAnAparitie("1890");
        carte.setEditura("Corint");
        List<String> cuvinteCheie = new ArrayList();
        cuvinteCheie.add("balaur");
        cuvinteCheie.add("printesa");
        carte.setCuvinteCheie(cuvinteCheie);
        int size = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(carte);
        Assert.assertEquals(size+1, cartiRepoMock.getCarti().size());
    }

    // testare unitara: B
    @Test
    public void unitTestB() {
        String autorCautat = "baco";
        List<Carte> result = cartiRepoMock.cautaCarte(autorCautat);
        Assert.assertEquals(0, result.size());
    }

    // testare unitara: C
    @Test
    public void unitTestC(){
        int anAparitie = 1973;
        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul(String.valueOf(anAparitie));
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("Poezii", result.get(0).getTitlu());


    }

    // testare de integrare A, B, C pentru a obtine P
    // scenariu de executie: P -> A -> C -> B
    @Test
    public void integrareTest() throws Exception {

        // A
        int count = cartiRepoMock.getCarti().size();
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Test;Emanuel;1992;Corint;awesome"));
        Assert.assertEquals(count + 1, cartiRepoMock.getCarti().size());

        // C
        List<Carte> result2 = cartiRepoMock.getCartiOrdonateDinAnul("1992");
        Assert.assertEquals(2, result2.size());
        Assert.assertEquals(2, result2.get(0).getReferenti().size());
        Assert.assertEquals("Calinescu", result2.get(0).getReferenti().get(0));

        // B
        List<Carte> result = cartiRepoMock.cautaCarte("manu");
        Assert.assertEquals(1, result.size());
    }
}
