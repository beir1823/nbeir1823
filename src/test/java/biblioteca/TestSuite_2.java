package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class TestSuite_2 {

    public static BibliotecaCtrl controller;

    @BeforeClass
    public static void instantiate()
    {
        controller = new BibliotecaCtrl(new CartiRepoMock());
        System.out.println("Once!!");
    }

    @Test
    public void testValidSearchBooksByAuthor1()
    {
        try
        {
            controller.cautaCarte("caragiale");
            assertTrue(true);
        }
        catch (Exception e)
        {
        }
    }

    @Test
    public void testInvalidSearchBooksByAuthor1()
    {
        try
        {
            controller.cautaCarte("ion carag2iale");
        }
        catch (Exception e)
        {
            assertTrue(true);
        }
    }

    @Test
    public void testValidSearchBooksByAuthor2()
    {
        try
        {
            List<Carte> rez = controller.cautaCarte("caragiale");
            assertEquals(3, rez.size());
        }
        catch (Exception e)
        {
        }
    }

    @Test
    public void testValidSearchBooksByAuthor3()
    {
        try
        {
            List<Carte> rez = controller.cautaCarte("Vasilica");
            assertEquals(0 , rez.size());
        }
        catch (Exception e)
    {
    }
    }

    @Test
    public void testInvalidSearchBooksByAuthor2()
    {
        try
        {
            List<Carte> rez = controller.cautaCarte("");
        }
        catch (Exception e)
        {
            assertTrue(true);
        }
    }
}
