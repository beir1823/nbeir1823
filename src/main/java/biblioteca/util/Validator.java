package biblioteca.util;

import biblioteca.model.Carte;
import com.sun.org.apache.bcel.internal.generic.I2F;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
//		boolean flag = s.matches("[a-zA-Z]+");
//		if(flag == false)
//			throw new Exception("String invalid");
//		return flag;

		String []t = s.split(" ");
		for (String st : t)
		{
			if (st.matches("[a-zA-Z]+") == false)
				throw new Exception("Titlu invalid!");
		}
		return true;
	}
	
	public static void validateCarte(Carte c)throws Exception{
	    StringBuilder errMsg = new StringBuilder();           		  //Added the StringBuilder!!
		if(c.getCuvinteCheie().size() == 0){                    	  // '(.size)' changed '==null' with '==0'
//			throw new Exception("Lista cuvinte cheie vida!");
            errMsg.append("Lista cuvinte cheie vida! \n");
		}
		if(c.getReferenti().size() == 0){                             // '(.size)' changed '==null' with '==0'
//			throw new Exception("Lista autori vida!");
            errMsg.append("Lista autori vida! \n");
		}
		if(!isOKString(c.getTitlu()))
//			throw new Exception("Titlu invalid!");
            errMsg.append("Titlu invalid! \n");
		for(String s:c.getReferenti()){
			if(!isOKString(s))
//				throw new Exception("Autor invalid!");
			    errMsg.append("Autor '" + s + "' invalid! \n");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isOKString(s))
//				throw new Exception("Cuvant cheie invalid!");
			    errMsg.append("Cuvant cheie '" + s + "' invalid! \n");
		}
		if(!Validator.isNumber(c.getAnAparitie()))
//			throw new Exception("An aparitie invalid!");
		    errMsg.append("An aparitie invalid! \n");
		if(!isOKString(c.getEditura()))
//			throw new Exception("Editura invalida!");
		    errMsg.append("Editura invalida! \n");

		if (errMsg.length() != 0)
        {
            throw new Exception(errMsg.toString());
        }
	}
	
	public static boolean isNumber(String s){
		return s.matches("[10-9]+");
	}
	
	public static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length >= 2){
			for(String st : t)									//changed to verify strings with more then 2 words!
			{
				if(st.matches("[a-zA-Z]+") == false)
				    return false;
			}
			return true;
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
